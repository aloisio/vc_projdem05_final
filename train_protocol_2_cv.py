import os


from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import train_test_split

from dnn_utils import get_train_df, image_augmentation_generator, image_generator
from dnn_utils import get_run, get_callbacks
from dnn_utils import get_resnet, unfreeze_resnet, get_incv3, unfreeze_incv3
from dnn_utils import get_pred_prob_acc, get_acc

from keras.callbacks import TensorBoard
import pandas as pd
import numpy as np


os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"]="1"

BATCH_SIZE = 64
CLASSES=102

log_dir='tb_mult3'


train_path = '101_ObjectCategories'
test_size = 10

run = get_run()


df_files = get_train_df(train_path)


results = pd.read_pickle("results.pkl")

for train_size in [30]: # [1,3.5, 10, 15, 20, 30]
    for set in range(3):

        df_train=pd.DataFrame()
        df_test=pd.DataFrame()

        for c in range(CLASSES):
            df_class = df_files.loc[df_files.class_encoded==c]
            df_train_class, df_test_class =  train_test_split(df_class, train_size = train_size)
            df_train = df_train.append(df_train_class)
            df_test = df_test.append(df_test_class)


        if train_size < 5:


            #############################
            print("inception v3")
            #############################
            k=0

            model, preproc_func = get_incv3()

            train_datagen = image_augmentation_generator(df_train, img_preproc_func=preproc_func, batch_size=BATCH_SIZE,
                                                         num_classes=CLASSES, augmentation=True)
            test_datagen = image_generator(df_test, img_preproc_func=preproc_func, batch_size=BATCH_SIZE,
                                           num_classes=CLASSES)

            model_name = 'incv3_tsize'+str(train_size)+'_set'+str(set)+'_fold'+str(k)+'-'+str(run)

            print("train", model_name)

            model.fit_generator(
                train_datagen,
                steps_per_epoch=int(np.ceil(len(df_train) / BATCH_SIZE)),
                epochs=50,
                callbacks=[TensorBoard(log_dir=log_dir + "/" + model_name)],
                workers=8)

            pred_prob, acc = get_pred_prob_acc(model, model_name, df_test, test_datagen, BATCH_SIZE)

            results = results.append(pd.DataFrame({'net': ['incv3'], 'train_size':[train_size], 'set':[set], 'acc': [acc]}),
                                     ignore_index=True)


            #############################
            print("resnet 50")
            #############################
            k=0

            model, preproc_func = get_resnet()

            train_datagen = image_augmentation_generator(df_train, img_preproc_func=preproc_func, batch_size=BATCH_SIZE,
                                                         num_classes=CLASSES, augmentation=True)
            test_datagen = image_generator(df_test, img_preproc_func=preproc_func, batch_size=BATCH_SIZE,
                                           num_classes=CLASSES)

            model_name = 'resnet50_tsize'+str(train_size)+'_set'+str(set)+'_fold'+str(k)+'-'+str(run)

            print("train", model_name)

            model.fit_generator(
                train_datagen,
                steps_per_epoch=int(np.ceil(len(df_train) / BATCH_SIZE)),
                epochs=30,
                callbacks=[TensorBoard(log_dir=log_dir + "/" + model_name)],
                workers=8)

            pred_prob, acc = get_pred_prob_acc(model, model_name, df_test, test_datagen, BATCH_SIZE)


            results = results.append(pd.DataFrame({'net': ['resnet50'], 'train_size':[train_size], 'set':[set], 'acc': [acc]}),
                                     ignore_index=True)

        else:

            incv3_acum_pred_prob = np.zeros((len(df_test), CLASSES))
            resnet_acum_pred_prob = np.zeros((len(df_test), CLASSES))

            skf = StratifiedKFold(n_splits=5, shuffle=True, random_state=111)

            k = 0

            for train_index, val_index in skf.split(df_train, df_train.class_encoded):
                train, val = df_train.iloc[train_index], df_train.iloc[val_index]

                k += 1


                #############################
                print("inception v3")
                #############################

                model, preproc_func = get_incv3()

                train_datagen = image_augmentation_generator(train, img_preproc_func=preproc_func, batch_size=BATCH_SIZE,num_classes=CLASSES, augmentation=True)
                valid_datagen = image_augmentation_generator(val, img_preproc_func=preproc_func, batch_size=BATCH_SIZE,num_classes=CLASSES)
                test_datagen = image_generator(df_test, img_preproc_func=preproc_func, batch_size=BATCH_SIZE,
                                               num_classes=CLASSES)

                model_name = 'incv3_tsize' + str(train_size) + '_set' + str(set) + '_fold' + str(k) + '-' + str(run)

                print("train", model_name)

                model.fit_generator(
                        train_datagen,
                        steps_per_epoch=int(np.ceil(len(train) / BATCH_SIZE)),
                        epochs=300,
                        validation_data=valid_datagen,
                        validation_steps=int(np.ceil(len(val) / BATCH_SIZE)),
                        callbacks=get_callbacks(model_name, log_dir=log_dir,patience=30),
                        workers=8 )

                pred_prob, acc = get_pred_prob_acc(model, model_name, df_test, test_datagen, BATCH_SIZE)

                incv3_acum_pred_prob = incv3_acum_pred_prob + pred_prob

                #############################
                print("resnet 50")
                #############################

                model, preproc_func = get_resnet()

                train_datagen = image_augmentation_generator(train, img_preproc_func=preproc_func, batch_size=BATCH_SIZE, num_classes=CLASSES, augmentation=True)
                valid_datagen = image_augmentation_generator(val, img_preproc_func=preproc_func, batch_size=BATCH_SIZE, num_classes=CLASSES)
                test_datagen = image_generator(df_test, img_preproc_func=preproc_func, batch_size=BATCH_SIZE,
                                               num_classes=CLASSES)

                model_name = 'resnet50_tsize' + str(train_size) + '_set' + str(set) + '_fold' + str(k) + '-' + str(run)

                print("train", model_name)

                model.fit_generator(
                        train_datagen,
                        steps_per_epoch=int(np.ceil(len(train) / BATCH_SIZE)),
                        epochs=300,
                        validation_data=valid_datagen,
                        validation_steps=int(np.ceil(len(val) / BATCH_SIZE)),
                        callbacks=get_callbacks(model_name, log_dir=log_dir,patience=30),
                        workers=8 )

                pred_prob, acc = get_pred_prob_acc(model, model_name, df_test, test_datagen, BATCH_SIZE)

                resnet_acum_pred_prob = incv3_acum_pred_prob + pred_prob

            print("predicting incv3 ensemble")

            acc = get_acc(incv3_acum_pred_prob, df_test)

            results = results.append(
                pd.DataFrame({'net': ['incv3'], 'train_size': [train_size], 'set': [set], 'acc': [acc]}),
                ignore_index=True)

            print("incv3 train_size:%d set:%d  acc=%.4f" % (train_size, set, acc))

            print("predicting resnet 50 ensemble")

            acc = get_acc(resnet_acum_pred_prob, df_test)

            results = results.append(
                pd.DataFrame({'net': ['resnet50'], 'train_size': [train_size], 'set': [set], 'acc': [acc]}),
                ignore_index=True)

            print("resnet train_size:%d set:%d  acc=%.4f" % (train_size, set, acc))

        results.to_pickle("results.pkl")