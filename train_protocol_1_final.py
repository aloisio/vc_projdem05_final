import os


from sklearn.model_selection import StratifiedKFold

from dnn_utils import get_train_test_df, image_generator
from dnn_utils import get_run, get_callbacks
from dnn_utils import get_resnet, unfreeze_resnet, get_incv3, unfreeze_incv3
from keras.callbacks import TensorBoard

os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"]="1"

BATCH_SIZE = 16
CLASSES=102


train_path = 'CalTech101/Train'
test_path = 'CalTech101/Test'
log_dir = "tensorboard"

df_train, df_test = get_train_test_df(train_path, test_path)

run = get_run()

#############################
print("inception v3")
#############################

model, preproc_func = get_incv3()

train_datagen = image_generator(df_train, img_preproc_func=preproc_func, batch_size=BATCH_SIZE,num_classes=CLASSES)
test_datagen = image_generator(df_test, img_preproc_func=preproc_func, batch_size=1,num_classes=CLASSES)

model_name= 'incv3_final_' + str(run)

model.fit_generator(
        train_datagen,
        steps_per_epoch=len(df_train)//BATCH_SIZE,
        epochs=18,
        callbacks=[TensorBoard(log_dir=log_dir+"/"+model_name)],
        workers=8 )

model_name= 'incv3_final_' + str(run) + "_fine"

unfreeze_incv3(model)

model.fit_generator(
        train_datagen,
        steps_per_epoch=len(df_train)//BATCH_SIZE,
        epochs=5,
        callbacks=[TensorBoard(log_dir=log_dir+"/"+model_name)],
        workers=8 )
model.save_weights(model_name+".hdf5")

#############################
print("resnet 50")
#############################

model, preproc_func = get_resnet()

train_datagen = image_generator(df_train, img_preproc_func=preproc_func, batch_size=BATCH_SIZE,num_classes=CLASSES)
test_datagen = image_generator(df_test, img_preproc_func=preproc_func, batch_size=1,num_classes=CLASSES)

model_name= 'resnet50_final' + "_" + str(run)

model.fit_generator(
        train_datagen,
        steps_per_epoch=len(df_train)//BATCH_SIZE,
        epochs=6,
        callbacks=[TensorBoard(log_dir=log_dir+"/"+model_name)],
        workers=8 )
model.save_weights(model_name+".hdf5")

model_name= 'resnet50_final' + "_" + str(run) + "_fine"

unfreeze_resnet(model)

model.fit_generator(
        train_datagen,
        steps_per_epoch=len(df_train)//BATCH_SIZE,
        epochs=3,
        callbacks=[TensorBoard(log_dir=log_dir+"/"+model_name)],
        workers=8 )

model.save_weights(model_name+".hdf5")