import pandas as pd
import matplotlib.pylab as plt

df_res = pd.read_pickle("results.pkl")
gp = df_res[['net','train_size','acc']].groupby(by=['net','train_size'],axis=0,as_index=False).agg(['mean','std']).reset_index()


for net,color,fmt in zip(gp.net.unique(),['blue','red'],['o-','d-']):
    gp_net = gp.loc[gp.net==net]

    p1= plt.errorbar(gp_net.train_size, gp_net.acc['mean'], yerr=gp_net.acc['std'], lw=1, ms=5, fmt=fmt,color=color,
                 ecolor=color, capsize=5,  label = net)
    #plt.locator_params(nbins=8)
    #plt.xlim([-1, 4])
plt.xticks(gp_net.train_size,gp_net.train_size)
plt.ylabel("Mean Accuracy")
plt.xlabel("Train Size")
plt.legend()
plt.show()

