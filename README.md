# Projeto Demonstrativo 5 - Classificação de Imagens

# Instruções

## Conteúdo
0. [Requisitos](##Requisitos)
0. [Estrutura de Pastas](##Estrutura de Pastas)
0. [Protocolo 1](##Protocolo 1)
0. [Protocolo 2](##Protocolo 2")

## Requisitos
1. Ubuntu 16.04 LTS ou Superior
2. Cuda 8 ou superior
3. Cudnn 6 ou superior
4. TensorFlow
5. Python 3.4 (numpy, pandas, scikitlearn, matplotlib, Pillow)
6. Keras
7. OpenCV 3


## Estrutura de Pastas
Para o treinamento as imagens devem ser disponibilizadas nos diretórios abaixo:

```Shell
    |--CalTech101               Imagens para o protocolo 1 - Não fornecidas
    |--101_ObjectCategories     Imagens para o protocolo 2 - Não fornecidas
```


## Protocolo 1

### Treinamento 

Para treinar as redes segundo o protocolo 1, siga os passos abaixo. O treinamento gera arquivos de peso (.hdf5) na pasta
corrente.

1. Treinamento em Cross-Validation sem Data Augmentation:

    ```shell
    python train_protocol_1_cv.py
    ```
    
2. Treinamento em Cross-Validation com Data augmentation:

    ```shell
    python train_protocol_1_aug_cv.py
    ```

3. Treinamento final com todas as imagens de treino e número de epochs ótimo, com e sem Data Augmentation:

    ```shell
    python train_protocol_1_final.py
    ```

### Avaliação e geração do gráfico (error bars) 

Para realizar a avaliação, é necessário treinar (e gerar os arquivos de pesos hdf5) antes.


  ```shell
  python eval_protocol_1.py
  ```


Caso gere novos arquivos de pesos, ajuste o número de execução no script, para que os arquivos corretos sejam usados: 
    

  ```shell
  #########################################
  #Ajustar  
  #
  run = 28
  run_aug = 30
  #########################################
  ```
    
O exemplo acima considera os seguintes arquivos de peso:

```shell
#5 folds sem augmentation
incv3_f1_28_fine.hdf5
incv3_f2_28_fine.hdf5
incv3_f3_28_fine.hdf5
incv3_f4_28_fine.hdf5
incv3_f5_28_fine.hdf5
resnet50_f1_28_fine.hdf5
resnet50_f2_28_fine.hdf5
resnet50_f3_28_fine.hdf5
resnet50_f4_28_fine.hdf5
resnet50_f5_28_fine.hdf5

#5 folds com augmentetion
incv3_aug_f1_30.hdf5
incv3_aug_f2_30.hdf5
incv3_aug_f3_30.hdf5
incv3_aug_f4_30.hdf5
incv3_aug_f5_30.hdf5
resnet50_aug_f1_30.hdf5
resnet50_aug_f2_30.hdf5
resnet50_aug_f3_30.hdf5
resnet50_aug_f4_30.hdf5
resnet50_aug_f5_30.hdf5

```



## Protocolo 2

### Treinamento 

Para treinar as redes segundo o protocolo 2 (**DEMORADO**), execute os scripts abaixo. O treinamento gera arquivos de peso (.hdf5) na pasta
corrente. No protocolo 2, a avaliaçao acontece concomitantemente com o treinamento. Os resultados são armazenados no arquivo 
pandas results.pkl. 

```shell
python train_protocol_2_cv.py
```
    
### Geração do gráfico (error bars) 

Para gerar o gráfico o arquivo results.pkl tem que ser gerado antes.


  ```shell
  python results_protocol_2.py
  ```

### Geração do gráfico de comparação com resultados anteriores 

Para gerar o gráfico o arquivo results.pkl tem que ser gerado antes.


  ```shell
  python results_compare.py
  ```
