import os


from sklearn.model_selection import StratifiedKFold

from dnn_utils import get_train_test_df, image_generator
from dnn_utils import get_run, get_callbacks
from dnn_utils import get_resnet, unfreeze_resnet, get_incv3, unfreeze_incv3

os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"]="1"

BATCH_SIZE = 16
CLASSES=102


train_path = 'CalTech101/Train'
test_path = 'CalTech101/Test'



df_train, df_test = get_train_test_df(train_path, test_path)

skf = StratifiedKFold(n_splits=5, shuffle=True, random_state=111)

run = get_run()

k = 0

models = []
for train_index, val_index in skf.split(df_train, df_train.class_encoded):
    train, val = df_train.iloc[train_index], df_train.iloc[val_index]

    k += 1


    #############################
    print("inception v3")
    #############################

    model, preproc_func = get_incv3()

    train_datagen = image_generator(train, img_preproc_func=preproc_func, batch_size=BATCH_SIZE,num_classes=CLASSES)
    valid_datagen = image_generator(val, img_preproc_func=preproc_func, batch_size=1,num_classes=CLASSES)
    test_datagen = image_generator(df_test, img_preproc_func=preproc_func, batch_size=1,num_classes=CLASSES)

    model_name= 'incv3_f' + str(k) + "_" + str(run)

    model.fit_generator(
            train_datagen,
            steps_per_epoch=len(train)//BATCH_SIZE,
            epochs=100,
            validation_data=valid_datagen,
            validation_steps=len(val),
            callbacks=get_callbacks(model_name, log_dir="tensorboard",patience=5),
            workers=8 )

    model.load_weights(model_name+'.hdf5')

    model_name= 'incv3_f' + str(k) + "_" + str(run) + "_fine"

    unfreeze_incv3(model)

    model.fit_generator(
            train_datagen,
            steps_per_epoch=len(train)//BATCH_SIZE,
            epochs=100,
            validation_data=valid_datagen,
            validation_steps=len(val),
            callbacks=get_callbacks(model_name, log_dir="tensorboard",patience=5),
            workers=8 )

    model.load_weights(model_name+'.hdf5')

    models.append(model)

    #############################
    print("resnet 50")
    #############################

    model, preproc_func = get_resnet()

    train_datagen = image_generator(train, img_preproc_func=preproc_func, batch_size=BATCH_SIZE,num_classes=CLASSES)
    valid_datagen = image_generator(val, img_preproc_func=preproc_func, batch_size=1,num_classes=CLASSES)
    test_datagen = image_generator(df_test, img_preproc_func=preproc_func, batch_size=1,num_classes=CLASSES)

    model_name= 'resnet50_f' + str(k) + "_" + str(run)

    model.fit_generator(
            train_datagen,
            steps_per_epoch=len(train)//BATCH_SIZE,
            epochs=100,
            validation_data=valid_datagen,
            validation_steps=len(val),
            callbacks=get_callbacks(model_name, log_dir="tensorboard",patience=5),
            workers=8 )


    model.load_weights(model_name+'.hdf5')

    model_name= 'resnet50_f' + str(k) + "_" + str(run) + "_fine"

    unfreeze_resnet(model)

    model.fit_generator(
            train_datagen,
            steps_per_epoch=len(train)//BATCH_SIZE,
            epochs=100,
            validation_data=valid_datagen,
            validation_steps=len(val),
            callbacks=get_callbacks(model_name, log_dir="tensorboard",patience=5),
            workers=8 )

    model.load_weights(model_name+'.hdf5')

    models.append(model)

