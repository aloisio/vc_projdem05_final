import pandas as pd
import matplotlib.pylab as plt

df_res = pd.read_pickle("results.pkl")

gp = df_res[['net','train_size','acc']].groupby(by=['net','train_size'],axis=0,as_index=False).mean()

gp = gp.append(pd.DataFrame({'net': 'Zhang, Berg, Maire, & Malik(CVPR06)',
                                     'train_size': [1,5,10, 15, 20, 30],
                                     'acc': [0.2250, 0.45, 0.55, 0.5905, 0.62, 0.6623]}),  ignore_index=True)


gp = gp.append(pd.DataFrame({'net': 'Mutch, & Lowe(CVPR06)',
                                     'train_size': [15, 30],
                                     'acc': [0.5100, 0.5600]}),  ignore_index=True)

gp = gp.append(pd.DataFrame({'net': 'Grauman & Darrell(ICCV05)',
                                     'train_size': [1,3,5,10,15, 20, 25, 30],
                                     'acc': [0.18, 0.28, 0.35, 0.44, 0.4953, 0.535, 0.56, 0.5823]}),  ignore_index=True)

print(gp.head(20))
for net,color,fmt in zip(gp.net.unique(),['blue','red', 'black', 'green', 'cyan'],['o-','d-', '^-', 'h-', '*-']):
    gp_net = gp.loc[gp.net==net]

    p1= plt.plot(gp_net.train_size, gp_net.acc,  fmt, lw=1, ms=5, color=color,
                  label = net)
    #plt.locator_params(nbins=8)
    #plt.xlim([-1, 4])
plt.xticks(gp_net.train_size,gp_net.train_size)
plt.ylabel("Mean Accuracy")
plt.xlabel("Train Size")
plt.legend()
plt.show()
