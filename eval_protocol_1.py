import os
os.environ['KERAS_BACKEND'] = 'tensorflow'
from dnn_utils import get_resnet, get_pred_prob_acc, get_acc, get_incv3
import numpy as np
import pandas as pd
from dnn_utils import get_train_test_df, image_generator

os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"]="0"


train_path = 'CalTech101/Train'
test_path = 'CalTech101/Test'

CLASSES=102


df_train, df_test = get_train_test_df(train_path, test_path)

TEST_BATCH_SIZE = len(df_test)//407

df_test['pred'] = 0

run = 28

run_aug = 30

results = pd.DataFrame()

incv3_acum_pred_prob = np.zeros((len(df_test),CLASSES))
resnet_acum_pred_prob = np.zeros((len(df_test),CLASSES))
incv3_aug_acum_pred_prob = np.zeros((len(df_test),CLASSES))
resnet_aug_acum_pred_prob = np.zeros((len(df_test),CLASSES))


for k in np.array(range(5))+1:

    print("\nPredicting fold",k)

    model_name= 'incv3_f' + str(k) + "_" + str(run) + '_fine'
    model, preproc_func = get_incv3()
    test_datagen = image_generator(df_test, img_preproc_func=preproc_func, batch_size=TEST_BATCH_SIZE,num_classes=CLASSES)

    pred_prob, acc = get_pred_prob_acc(model, model_name, df_test, test_datagen, TEST_BATCH_SIZE)

    print("Inception V3 acc=", acc)

    results = results.append(pd.DataFrame({'net':['incv3'], 'acc':[acc], 'fold':[k]}),ignore_index=True)

    incv3_acum_pred_prob = incv3_acum_pred_prob + pred_prob

    model_name= 'resnet50_f' + str(k) + "_" + str(run) + '_fine'
    model, preproc_func = get_resnet()
    test_datagen = image_generator(df_test, img_preproc_func=preproc_func, batch_size=TEST_BATCH_SIZE,num_classes=CLASSES)

    pred_prob, acc = get_pred_prob_acc(model, model_name, df_test, test_datagen, TEST_BATCH_SIZE)

    print("Resnet acc=", acc)

    results = results.append(pd.DataFrame({'net':['resnet50'], 'acc':[acc], 'fold':[k]}),ignore_index=True)

    resnet_acum_pred_prob = resnet_acum_pred_prob + pred_prob


    #aug

    model_name= 'incv3_aug_f' + str(k) + "_" + str(run_aug) + '_fine'
    model, preproc_func = get_incv3()
    test_datagen = image_generator(df_test, img_preproc_func=preproc_func, batch_size=TEST_BATCH_SIZE,num_classes=CLASSES)

    pred_prob, acc = get_pred_prob_acc(model, model_name, df_test, test_datagen, TEST_BATCH_SIZE)

    print("Inception V3 Augmented acc=", acc)

    results = results.append(pd.DataFrame({'net':['incv3_aug'], 'acc':[acc], 'fold':[k]}),ignore_index=True)

    incv3_aug_acum_pred_prob = incv3_acum_pred_prob + pred_prob

    model_name= 'resnet50_aug_f' + str(k) + "_" + str(run_aug) + '_fine'
    model, preproc_func = get_resnet()
    test_datagen = image_generator(df_test, img_preproc_func=preproc_func, batch_size=TEST_BATCH_SIZE,num_classes=CLASSES)

    pred_prob, acc = get_pred_prob_acc(model, model_name, df_test, test_datagen, TEST_BATCH_SIZE)

    print("Resnet Augmented acc=", acc)

    results = results.append(pd.DataFrame({'net':['resnet50_aug'], 'acc':[acc], 'fold':[k]}),ignore_index=True)

    resnet_aug_acum_pred_prob = resnet_acum_pred_prob + pred_prob



combined_pred_prob = (incv3_acum_pred_prob + resnet_acum_pred_prob)/2
combined_aug_pred_prob = (incv3_aug_acum_pred_prob + resnet_aug_acum_pred_prob)/2

acc = get_acc(incv3_acum_pred_prob, df_test)

print("\nInception V3 ensemble acc:", acc)

acc = get_acc(resnet_acum_pred_prob, df_test)

print("\nResnet 50 ensemble acc:", acc)

acc = get_acc(incv3_aug_acum_pred_prob, df_test)

print("\nInception Augmented V3 ensemble acc:", acc)

acc = get_acc(resnet_aug_acum_pred_prob, df_test)

print("\nResnet Augmented 50 ensemble acc:", acc)

acc = get_acc(combined_pred_prob, df_test)

print("\nCombined Inception + Resnet acc:", acc)

acc = get_acc(combined_aug_pred_prob, df_test)

print("\nCombined Augmented Inception + Resnet acc:", acc)


print(results.head(20))

gp = results[['net','acc']].groupby(by='net',axis=0,as_index=False)

gp_df = gp['acc'].agg(['mean','std']).reset_index()



#import pandas as pd
#import numpy as np

#gp_df = pd.DataFrame({'net':['inc','res','1','3'], 'mean':[1,2,3,4], 'std':[0.1, 0.2, 0.1,0.2]})

import matplotlib.pylab as plt


#p1 = plt.bar(np.arange(2), gp_df['mean'], 0.8, yerr=gp_df['std'],color='white',edgecolor=['black','black'], align='center', alpha=0.5, ecolor='black', capsize=15)

p1= plt.errorbar(np.arange(4), gp_df['mean'], yerr=gp_df['std'], fmt='o',color='black',
                 ecolor='black', capsize=15)
plt.locator_params(nbins=8)
plt.xlim([-1, 4])
plt.xticks(np.arange(4), gp_df['net'])
plt.show()

# final

model_name = 'incv3_final_34_fine'
model, preproc_func = get_incv3()
test_datagen = image_generator(df_test, img_preproc_func=preproc_func, batch_size=TEST_BATCH_SIZE, num_classes=CLASSES)

pred_prob, acc = get_pred_prob_acc(model, model_name, df_test, test_datagen, TEST_BATCH_SIZE)

print("Inception V3 Final acc=", acc)

model_name = 'resnet50_final_34_fine'
model, preproc_func = get_resnet()
test_datagen = image_generator(df_test, img_preproc_func=preproc_func, batch_size=TEST_BATCH_SIZE, num_classes=CLASSES)

pred_prob, acc = get_pred_prob_acc(model, model_name, df_test, test_datagen, TEST_BATCH_SIZE)

print("Resnet Final acc=", acc)

# final aug

model_name = 'incv3_aug_final_35_fine'
model, preproc_func = get_incv3()
test_datagen = image_generator(df_test, img_preproc_func=preproc_func, batch_size=TEST_BATCH_SIZE, num_classes=CLASSES)

pred_prob, acc = get_pred_prob_acc(model, model_name, df_test, test_datagen, TEST_BATCH_SIZE)

print("Inception V3 Augmented Final acc=", acc)

model_name = 'resnet50_aug_final_35_fine'
model, preproc_func = get_resnet()
test_datagen = image_generator(df_test, img_preproc_func=preproc_func, batch_size=TEST_BATCH_SIZE, num_classes=CLASSES)

pred_prob, acc = get_pred_prob_acc(model, model_name, df_test, test_datagen, TEST_BATCH_SIZE)

print("Resnet Augmented Final acc=", acc)
